//
//  ViewController.m
//  learning
//
//  Created by mohd zharif anuar on 04/04/2017.
//  Copyright © 2017 mohd zharif anuar. All rights reserved.
//

#import "ViewController.h"

#import "SecondPage.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Learn Git";
    
    lblDisplay.text = @"This is training using git app.\nI love to do programming.\nProgramming is something really nice to learn.";
}

-(IBAction)btnNavigationTapped:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    SecondPage *noname = [storyBoard instantiateViewControllerWithIdentifier:@"SecondPage"];
    [self showViewController:noname sender:self];
}

-(IBAction)btnDisplayTapped:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Learning"
                                                                   message:@"Welcome to LearnGit"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"Ok"
                                                    style:UIAlertActionStyleDefault
                                                  handler:nil];
    [alert addAction:btnOk];
    [self presentViewController:alert
                       animated:true
                     completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

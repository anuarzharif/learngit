//
//  SecondPage.m
//  learning
//
//  Created by mohd zharif anuar on 04/04/2017.
//  Copyright © 2017 mohd zharif anuar. All rights reserved.
//

#import "SecondPage.h"

@interface SecondPage ()

@end

@implementation SecondPage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Second Page";
}

-(IBAction)btnDisplayTapped:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Learning"
                                                                   message:@"Welcome to SecondPage"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"Ok"
                                                    style:UIAlertActionStyleDefault
                                                  handler:nil];
    [alert addAction:btnOk];
    [self presentViewController:alert
                       animated:true
                     completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

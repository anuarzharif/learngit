//
//  main.m
//  learning
//
//  Created by mohd zharif anuar on 04/04/2017.
//  Copyright © 2017 mohd zharif anuar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

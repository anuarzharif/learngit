//
//  AppDelegate.h
//  learning
//
//  Created by mohd zharif anuar on 04/04/2017.
//  Copyright © 2017 mohd zharif anuar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

